'use strict';

module.exports = { randomInt,
                   shuffle
                 };

function randomInt(lo,hi) {
  let number = Math.random() * (hi - lo) + lo;
  let value = Math.floor(number);
  return value;
}

function shuffle(names){
  let currentIndex = names.length;
  while(currentIndex !== 0) {
    let randomIndex = randomInt(0,names.length);
    currentIndex--;

    let tempvalue = names[currentIndex];
    names[currentIndex] = names[randomIndex];
    names[randomIndex] = tempvalue;
  }
}
