$(document).ready(function() {

  // ready for Javascript which will run when the html is loaded

  const faces  =  $('faces');
  //const add = $ ('<b>').html('Hello');
//const name = "Thiru";
const makeFace = function(name) {
  return $('<img>', {
    title:name,
    src:`img/2018/${name}.jpg `,
    class:"face"
  });
};
const names = ["Akshat","Akhil","Aishu","Sindhu","Varsha","Supriya","Rishi","Ameya","John","Jon","Karthika","Keerthana","Sudeep","Santhosh","Mariah","Janani",
"Sanjana","Shruti","Gayatri","Vishnu","RVishnuPriya","Pavithrann","Anushree","Thamizh","Thiru","Mahidher"];
names.forEach(function(name) {
    faces.append(makeFace(name));
  });
});
